import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableHighlight } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default function Support() {
  const [message, setSupport] = useState('')

  const doSubmit = () => {
    const data = {
      user_id:2,
      message: message
    };
    fetch('https://ezzyin.ausnep.com/api/business/support', {
      method: 'POST', // or 'PUT'
      headers: {
        'accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then(response => response.json())
      .then(data => {
        console.log('Support:', data);
        setSupport('')
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  }

  return (
    <View style={styles.container}>

      <View style={styles.textareastyle}>
        <TextInput
          style={styles.textArea}
          underlineColorAndroid="transparent"
          placeholder="  Message"
          placeholderTextColor="grey"
          numberOfLines={3}
          multiline={true}
          value={message}
          onChangeText={message => setSupport(message)}
          clearButtonMode='always'
        />
      </View>

      <View style={styles.btnstyle}>
        <TouchableHighlight style={styles.loginBtn} onPress={() => doSubmit()}>
          <Text style={styles.loginText}>SUBMIT</Text>
        </TouchableHighlight>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'white'
  },

  textareastyle: {
    // flex:1,
    height: hp('20%'),
    width: wp('90%'),
    marginTop: hp('5%'),
    borderColor: 'gray',
    borderWidth: hp('0.1%'),
    borderRadius: wp('2%'),
  },
  textArea: {
    justifyContent: "flex-start",
    fontSize: wp('4%'),

  },
  btnstyle: {
    // flex:2,
    height: hp('62%'),
    width: wp("90%"),
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginBottom: hp('10%')
  },
  loginBtn: {
    backgroundColor: '#696969',
    paddingTop: hp("1.5%"),
    paddingLeft: wp('2.5%'),
    paddingRight: wp('2.5%'),
    paddingBottom: hp('1.5%'),
    width: "100%",
    borderRadius: wp('3.5%')
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: wp('3%')
  },


})