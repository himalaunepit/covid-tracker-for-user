import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  Alert,
  ScrollView,
  FlatList,
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default class RecentActivity extends Component {

  constructor(props) {
    super(props);
    this.state = {
      calls: [
        {id:1,  name: "Himal Rawal",    status:"6 Min Ago", image:"https://bootdey.com/img/Content/avatar/avatar6.png"},
        {id:2,  name: "Sandesh Shah",   status:"1 Hour Ago", image:"https://bootdey.com/img/Content/avatar/avatar6.png"} ,
      
      ]
    };
  }

  renderItem = ({item}) => {
    return (
    <View style={{flex:1,justifyContent:'flex-start',alignItems:'center'}}>
        <View style={styles.row}>
            <View style={styles.picView}>
            <Image source={{ uri: item.image }} style={styles.pic} />
            </View>
          <View>
           
            <View style={styles.nameContainer}>
              <Text style={styles.name} numberOfLines={1} ellipsizeMode="tail">{item.name}</Text>
            </View>
            <View style={styles.nameContainer}>
                  <Text style={styles.nameTxt}>Himal Rawal ,Point your Camera </Text>
              </View>
            <View style={styles.nameContainer}>
              <Text style={styles.msgTxt}>{item.status}</Text>
            </View>
          </View>
        </View>
        </View>
    );
  }

  render() {
    return(
      <View style={{ flex: 1,backgroundColor:'white'}} >
        <FlatList 
          extraData={this.state}
          data={this.state.calls}
          keyExtractor = { (item, index) => index.toString() }
          renderItem={this.renderItem}/>
           <View style={styles.btnstyle}>
                <TouchableHighlight style={styles.loginBtn}>
                        <Text style={styles.loginText}>CLOSE</Text>
                </TouchableHighlight>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#DCDCDC',
    borderBottomColor:'#e6e6e6',
    borderBottomWidth:wp('0.4%'),
    padding: wp('4%'),
    width:wp('90%'),
    alignItems:'center',
    justifyContent:'center'
  },
  pic: {
    borderRadius:38,
    width: 76,
    height: 76,
  },
  picView:{
    borderRadius: 45,
    width: 90,
    height: 90,
    borderColor:'gray',
    borderWidth:2,
    justifyContent:'center',
    alignItems:'center'
  },
  nameContainer: {
    flexDirection: 'column',
    marginTop:hp('2%'),
    marginLeft:wp('5%'),
    justifyContent: 'space-between',
    width:wp('60%'),
  },
  name:{
      fontSize:wp('4%'),
      fontWeight:'bold',
  },
  btnstyle:{
    width:wp("80%"),
    justifyContent:'flex-end',
    alignItems:'center',
    marginLeft:wp('10%'),
    marginBottom:hp('6%'),
  },
  loginBtn:{
      backgroundColor:'#696969', 
      paddingTop:hp("1.5%"), 
      paddingLeft:wp('2.5%'), 
      paddingRight:wp('2.5%'),
      paddingBottom:hp('1.5%'),
      width:"100%",
      borderRadius:wp('3.5%'),
      alignItems:'center'
    },
    loginText:{
      color:'white', 
      fontWeight:'bold',
      textAlign:'center',
      fontSize:wp('3%')
    },
    msgTxt:{
        fontSize:wp('3%'),
        fontWeight:'bold',
        color:'gray'
    }
 
});