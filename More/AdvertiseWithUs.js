import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableHighlight } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default function AdvertiseWithUs()  {
  
  const[subject,setSubject]=useState('')
  const[message,setMessage]=useState('')
  
  const doSubmit = () => {
    const data = {
      user_id:2,
      subject,
      message,
    };
    fetch('https://ezzyin.ausnep.com/api/business/adsrequest', {
      method: 'POST', // or 'PUT'
      headers: {
        'accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then(response => response.json())
      .then(data => {
        console.log('Add:', data);
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  }

    return (
      <View style={styles.container}>
        <View style={styles.mainContainer}>
    
          <View style={styles.textinput}>
            <TextInput
              style={styles.inputText}
              placeholder=" Subject  "
              placeholderTextColor="#A9A8A6"
              value={subject}
              clearButtonMode='always'
              onChangeText={subject => setSubject(subject)} />
          </View>

          <View style={styles.textareastyle}>
            <TextInput
              style={styles.textArea}
              underlineColorAndroid="transparent"
              placeholder="  Message"
              placeholderTextColor="grey"
              numberOfLines={4}
              multiline={true}
              value={message}
              clearButtonMode='always'
              onChangeText={message => setMessage(message)}
            />
          </View>

          <View style={styles.btnstyle}>
            <TouchableHighlight style={styles.loginBtn} onPress={()=>doSubmit()}>
              <Text style={styles.loginText}>SUBMIT</Text>
            </TouchableHighlight>
          </View>

        </View>

      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    // flex:1,
    height: hp('100%'),
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  mainContainer: {
    marginTop: hp('3%')
  },

  textinput: {
    // flex:0.7,
    height: hp('8%'),
    width: wp('90%'),
    justifyContent: 'center',
  },
  inputText: {
    height: hp('5%'),
    color: "#808080",
    borderColor: 'gray',
    borderWidth: hp('0.1%'),
    borderRadius: wp('1%'),
    fontSize: wp('4%'),
  },
  textareastyle: {
    // flex:2,
    height: hp('15%'),
    width: wp('90%'),
    borderColor: 'gray',
    borderWidth: hp('0.1%'),
    borderRadius: wp('1%'),
    marginTop: hp('2%'),
  },
  textArea: {
    justifyContent: "flex-start",
    fontSize: wp('4%'),

  },
  btnstyle: {
    // flex:1,
    height: hp('57%'),
    width: wp("90%"),
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  loginBtn: {
    backgroundColor: '#696969',
    paddingTop: hp("1.7%"),
    paddingLeft: wp('2.7%'),
    paddingRight: wp('2.7%'),
    paddingBottom: hp('1.7%'),
    width: "100%",
    borderRadius: wp('3.5%')
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: wp('3%')
  },

})