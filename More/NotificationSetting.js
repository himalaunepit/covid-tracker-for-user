import { MaterialCommunityIcons } from '@expo/vector-icons';
import React,{useState} from 'react';
import { StyleSheet, Text, View, TextInput, TouchableHighlight,Switch } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
function NotificationSetting(){
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled(previousState => !previousState);
    return (
      <View style={styles.container}>
          <View style={styles.mainheader}>
          <Text style={styles.textStyle}>New Message Notifications</Text>
          <Switch
            trackColor={{ false: "#767577", true: "#81b0ff" }}
            thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
            ios_backgroundColor="#3e3e3e"
            onValueChange={toggleSwitch}
            style={{ transform: [{ scaleX: 1.5 }, { scaleY: 1.5 }] }}
            value={isEnabled}
      />
          </View>
     
      </View>
    );
  }
export default NotificationSetting
const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'flex-start',
        alignItems:'center',
        backgroundColor:'white',
        
    },
    mainheader:{
          flexDirection:'row',
          justifyContent:'space-between',
          height:hp('9%'),
          width:wp('70%')
    },
    textStyle:{
    fontSize:wp('4%'),
    marginTop:hp('3%'),
    fontWeight:'bold'
    },

})