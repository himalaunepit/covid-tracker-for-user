import React from 'react';
import { StyleSheet,Text,Image,TouchableOpacity } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { View } from 'native-base';
import { MaterialCommunityIcons } from '@expo/vector-icons';


const slides = [
  {
    key: '1',
    title: 'Scan Your Profile',
    text: 'We make it simple to find the latest deal for you .Enter your address ',
    image: require('../assets/onboardingscreen.png'),
    backgroundColor: '#59b2ab',
  },
  {
    key: '2',
    title: 'Get exclusive ',
    text: 'When you buy deals,we will hook you up with exclusive cupon,specials',
    image: require('../assets/Bitmap.png'),
    backgroundColor: '#febe29',
  },
  {
    key: '3',
    title: 'Pick Up Or',
    text: 'We make food ordering fast,simple and free,no matter if you order',
    image: require('../assets/Layer.png'),
    backgroundColor: '#22bcb5',
  }
];

export default class Onboarding extends React.Component {
    constructor(){
        super();
            this.state = {
                showRealApp: false
              }
        }

  _renderItem = ({ item }) => {
    return (
      <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'white'}}>
          <TouchableOpacity onPress={()=>this.props.navigation.navigate("RegisterScreen")}style={{flexDirection:'row',height:hp('10%'),justifyContent:'flex-end',marginTop:hp('5%'),width:wp('80%'),marginLeft:wp('12%')}}>
              <Text style={{color:'#15B2A2',fontSize:wp('4%')}}>Skip</Text>
              <MaterialCommunityIcons name='chevron-double-right' color={'#15B2A2'} size={wp('5%')} style={{marginTop:hp('0.4%')}}/>
          </TouchableOpacity>
           <Image source={item.image} style={styles.image} />
           <Text style={styles.title}>{item.title}</Text>
          <Text style={styles.text}>{item.text}</Text>
          <View style={{height:hp('25%'),justifyContent:'center'}}>
          { item.key == 3 &&(
              
              <TouchableOpacity style={styles.loginBtn} onPress={()=>this.props.navigation.navigate("RegisterScreen")}>
              <Text style={styles.loginText}>GET STARTED</Text>
           </TouchableOpacity>
          )}
          </View>
      </View>
    );
  }
  _onDone = () => {
    // User finished the introduction. Show real app through
    // navigation or simply by controlling state
    this.setState({ showRealApp: true });
  }
  render() {
    if (this.state.showRealApp) {
      return <App />;
    } else {
      return <AppIntroSlider renderItem={this._renderItem} data={slides} onDone={this._onDone}
      activeDotStyle={{marginLeft:wp('-2%'),width:wp('10%'),backgroundColor:'#696969',height:hp('0.8%')}}
      dotStyle={{marginLeft:wp('-2%'),width:wp('10%'),backgroundColor:'#e6e6e6',height:hp('0.8%')}}
      />;
    }
  }
}
const styles = StyleSheet.create({
    title:{
        marginTop:hp('5%'),
        height:hp('8%'),
        fontSize:wp('9%'),
        fontWeight:'bold',
        justifyContent:'flex-end',
        alignItems:'center',
        color:'#262628'
    },
    text:{
        height:hp('8%'),
        fontSize:wp('3.5%'),
        marginTop:hp('5%'),
        justifyContent:'flex-end',
        alignItems:'center',
        width:wp('60%'),
        color:'#C0C1C9'
    },
    loginBtn:{
        width:wp('80%'),
        backgroundColor:'#696969', 
        paddingTop:hp("1.5%"), 
        paddingLeft:wp('2.5%'), 
        paddingRight:wp('2.5%'),
        paddingBottom:hp('1.5%'),
        borderRadius:wp('3.5%'),
       
      },
      loginText:{
        color:'white', 
        fontWeight:'bold',
        textAlign:'center',
        fontSize:wp('3%')
      },
      image:{
          height:hp('35%'),
          justifyContent:'flex-end',
          alignItems:'center'
      }
})