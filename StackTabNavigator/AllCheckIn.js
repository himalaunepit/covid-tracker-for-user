import React from 'react'
import {Text,View,StyleSheet,TouchableOpacity} from 'react-native'
import {FontAwesome} from '@expo/vector-icons'
import Constants from 'expo-constants';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
function AllCheckIn(){
  return(
    <View style={styles.container}>
      <View style={styles.mainCard}>
          <View style={{margin:12}}>
          <Text style={styles.name}>
             Himal Rawal
         </Text>
         <Text style={styles.email}>
            HimalRawal@gmail.com
         </Text>
         <View style={styles.phoneTime}>
             <Text style={{color:'#C1C0C9'}}>9860552717</Text>
             <Text style={{color:'#C1C0C9'}}>6 mins ago</Text>
         </View>
          </View>
      </View>

      <View style={styles.mainCard}>
          <View style={{margin:12}}>
          <Text style={styles.name}>
             Bibek Chettri
         </Text>
         <Text style={styles.email}>
            HimalRawal@gmail.com
         </Text>
         <View style={styles.phoneTime}>
             <Text style={{color:'#C1C0C9'}}>9860552717</Text>
             <Text style={{color:'#C1C0C9'}}>1 mins ago</Text>
         </View>
          </View>
      </View>

      <View style={styles.mainCard}>
          <View style={{margin:12}}>
          <Text style={styles.name}>
             Sandip Tiwari
         </Text>
         <Text style={styles.email}>
            HimalRawal@gmail.com
         </Text>
         <View style={styles.phoneTime}>
             <Text style={{color:'#C1C0C9'}}>9860552717</Text>
             <Text style={{color:'#C1C0C9'}}>19 mins ago</Text>
         </View>
          </View>
      </View>

      <View style={styles.mainCard}>
          <View style={{margin:12}}>
          <Text style={styles.name}>
             Shreeyana Kadel
         </Text>
         <Text style={styles.email}>
            HimalRawal@gmail.com
         </Text>
         <View style={styles.phoneTime}>
             <Text style={{color:'#C1C0C9'}}>9860552717</Text>
             <Text style={{color:'#C1C0C9'}}>19 mins ago</Text>
         </View>
          </View>
      </View>

      <View style={styles.mainCard}>
          <View style={{margin:12}}>
          <Text style={styles.name}>
             Ramesh Bista
         </Text>
         <Text style={styles.email}>
            HimalRawal@gmail.com
         </Text>
         <View style={styles.phoneTime}>
             <Text style={{color:'#C1C0C9'}}>9860552717</Text>
             <Text style={{color:'#C1C0C9'}}>19 mins ago</Text>
         </View>
          </View>
      </View>

    </View>
  )
}
export default AllCheckIn

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'white',
    marginTop:15,

 
  },
  mainCard:{
    borderColor:'white',
    borderRadius:hp('2%'),
    elevation:7,
    backgroundColor:'white',
    marginVertical:15
  },
phoneTime:{
    flexDirection:'row',
    justifyContent:'space-between',
 
},
email:{
    marginTop:hp('1%'),
    color:'#C1C0C9'
},
name:{
    fontSize:16,
    fontWeight:'bold'
}
 
})