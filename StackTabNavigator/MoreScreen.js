import * as React from 'react';
import { View,Text,StyleSheet,TouchableOpacity } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
function MoreScreen({navigation}) {
  return (
    <View style={styles.container}>
        <View style={{width:wp('90%'),marginTop:hp('2%')}}>
        <View style={styles.headerStyle}>
                    <Text style={{fontWeight:'bold',fontSize:wp('4%'),marginBottom:hp('1%')}}>More</Text>
        </View>
               <TouchableOpacity  style={styles.lineItem} onPress={
            () => navigation.navigate('NotificationSetting')}>
               <MaterialCommunityIcons name="bell"   size={wp('6%')} />
                <Text style={styles.itemname}>Notification Setting</Text>
                <MaterialCommunityIcons name="chevron-right"  size={wp('6%')} style={{marginLeft:wp('43%')}} />
               </TouchableOpacity>
            
        
            <TouchableOpacity style={styles.lineItem}  onPress={
            () => navigation.navigate('AdvertiseWithUs')}>
            <MaterialCommunityIcons name="contactless-payment"  size={wp('6%')}/>
                <Text style={styles.itemname}>Advertise With Us</Text>
                <MaterialCommunityIcons name="chevron-right"  size={wp('6%')} style={{marginLeft:wp('46%')}}/>
            </TouchableOpacity>
           <TouchableOpacity style={styles.lineItem} onPress={
            () => navigation.navigate('Support')}>
           <MaterialCommunityIcons name="settings-outline"   size={wp('6%')} />
                <Text style={styles.itemname} >Support</Text>
                <MaterialCommunityIcons name="chevron-right"   size={wp('6%')} style={{marginLeft:wp('63%%')}} />
           </TouchableOpacity>
        
        <TouchableOpacity style={styles.lineItem} onPress={
            () => navigation.navigate('PrivacyPolicy')}>
        <MaterialCommunityIcons name="lock"   size={wp('6%')} />
                <Text style={styles.itemname} > Privacy Policy</Text>
                <MaterialCommunityIcons name="chevron-right"   size={wp('6%')} style={{marginLeft:wp('51%')}} />
        </TouchableOpacity>

        <TouchableOpacity style={styles.lineItem} onPress={
            () => navigation.navigate('TermsOfUse')}>
               <MaterialCommunityIcons name="alert-octagram"   size={wp('6%')} />
                <Text style={styles.itemname} >Terms of Use</Text>
                <MaterialCommunityIcons name="chevron-right"  size={wp('6%')} style={{marginLeft:wp('52%')}} />
        </TouchableOpacity>
        
              <TouchableOpacity  style={styles.lineItemlogout} onPress={()=>navigation.navigate('LoginScreen')}>
              <MaterialCommunityIcons name="logout"  size={wp('6%')} />
                <Text style={styles.itemname} >Log Out</Text>
              </TouchableOpacity>

        </View>
    </View>
  );
}
export default MoreScreen

const styles = StyleSheet.create({
    container:{
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'flex-start',
        backgroundColor:'white'
    },
    lineItem:{
        flexDirection:'row',
        width:wp('90%'),
        alignItems:'center',
        height:hp('8%'),
        borderBottomColor:'#C1C0C9',
        borderBottomWidth:hp('0.1%')
    },
    headerStyle:{
        height:hp('7%'),
        justifyContent:'flex-end',
        alignItems:'center',
        width:wp('90%'),
        marginTop:hp('1%'),
        borderBottomColor:'#e6e6e6',
        borderBottomWidth:wp('0.4%')
      
      },
    lineItemlogout:{
        flexDirection:'row',
        width:wp('90%'),
        alignItems:'center',
        height:hp('8%'),
        borderBottomColor:'white',
        borderBottomWidth:hp('0.1%')
    },
    itemname:{
       marginLeft:wp('2%'),
       fontSize:wp('4%')
    }
})
