import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  FlatList,
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default class ContactsScreen extends Component {
     twoOptionAlertHandler = () => {
        //function to make two option alert
        Alert.alert(
          //title
          'Details',
          //body
          'Himal Rawal himalrawal@gmail.com 9860552717 Tikapur,Kailali ', 
          
          [
            {
              text: 'Contact',
              onPress: () => console.log('Contact Pressed')
            },
            {
              text: 'Email',
              onPress: () => console.log('Email Pressed'), style: 'cancel'
            },
          ],
          {cancelable: false},
          //clicking out side of alert will not cancel
        );
      };
  constructor(props) {
    super(props);
    this.state = {
      calls: [
        {id:1,  name: "Himal Rawal"   },
        {id:2,  name: "Ramesh Bista",  } ,
        {id:3,  name: "Bibek Chetri",  } ,
        {id:4,  name: "Himal Rawal",  } ,
        {id:5,  name: "Shreeyana Kadel",   } ,
        {id:6,  name: "Prabat Swar" } ,
        {id:8,  name: "Prakash Rawal", } ,
        {id:9,  name: "Dharam Rawal",    } ,
        {id:10, name: "Santosh Pandey", } ,
        {id:11, name: "Shyam Khanal",   },
      ]
    };
  }

  renderItem = ({item}) => {
    return (
     
        <View style={styles.container}>
            <View style={{marginTop:hp('5%')}}>
                    <View style={styles.nameContainer}>
                    <Text style={styles.nameTxt} numberOfLines={1} ellipsizeMode="tail">{item.name}</Text>
                    <TouchableOpacity>
                    <Text style={styles.mblTxt}  onPress={()=>this.twoOptionAlertHandler()}>view detail</Text>
                    </TouchableOpacity>
                    </View>
            </View>
        </View>

    );
  }

  render() {
    return(
      <View style={{ flex: 1 }} >
        <View style={{justifyContent:'center',alignItems:'center',height:hp('6%'),borderBottomColor:'#C1C0C9',borderBottomWidth:2,marginTop:hp('5%')}}>
          <Text style={{fontSize:wp('4%'),fontWeight:'bold'}}>Contacts</Text>
        </View>
        <FlatList 
          showsVerticalScrollIndicator ={false}
          extraData={this.state}
          data={this.state.calls}
          // keyExtractor = {(item) => {
          //   return item.id;
          // }}
          keyExtractor = { (item, index) => index.toString() }
          renderItem={this.renderItem}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
      justifyContent:'flex-start',
      alignItems:'center',
  },
  nameContainer:{
      flexDirection:'row',
      justifyContent:'space-between',
      width:wp('80%'),
      height:hp('5%'),
      borderBottomColor:'#C1C0C9',
      borderBottomWidth:hp('0.1%')
  },
  nameTxt:{
      fontSize:wp('4%'),
      fontWeight:'bold'
  },
  headerStyle:{
    height:hp('10%'),
    justifyContent:'flex-end',
    alignItems:'center'
  }

});