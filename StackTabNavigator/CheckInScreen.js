import React,{useState}from 'react'
import {Text,View,StyleSheet,TouchableOpacity,Button} from 'react-native'
import {FontAwesome} from '@expo/vector-icons'
import Constants from 'expo-constants';
import TodayCheckIn from './TodayCheckIn'
import AllCheckIn from './AllCheckIn'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
function CheckInScreen(){
  const [isAllToday,setIsAllToday] =useState(false)

  return(
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.headerText}>
          Check In
        </Text>
        <TouchableOpacity>
          <FontAwesome name="filter" size={16}/>
          <Text style={{fontSize:12}}>filter</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.btnHeader}>
        <TouchableOpacity style={styles.leftButton} onPress={()=>setIsAllToday(false) } >
          <Text style={{color:'gray'}}>Today</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.rightButton} onPress={()=>setIsAllToday(true)}>
          <Text style={styles.btnText}>All</Text>
        </TouchableOpacity>
      </View>
      {isAllToday==true?<AllCheckIn/>:<TodayCheckIn/>}
    </View>
  )
}
export default CheckInScreen

const styles = StyleSheet.create({
  container:{
    flex:1,
    marginTop:Constants.statusBarHeight,
    backgroundColor:'white',
    paddingHorizontal:wp('8%')
 
  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    borderBottomColor:'#C1C0C9',
    borderBottomWidth:1,
    height:hp('7%'),
    alignItems:'center',
  
  },
  headerText:{
    fontSize:wp('5%'),
    fontWeight:'bold',
    marginLeft:wp('32%')
  },
  btnHeader:{
    flexDirection:'row',
    justifyContent:'space-between',

  },
  leftButton:{
    height:hp('5%'),
    backgroundColor:'#C1C0C9',
    width:wp('42%'),
    justifyContent:'center',
    alignItems:'center',
    borderBottomLeftRadius:wp('3%')
  },
  rightButton:{
    height:hp('5%'),
    backgroundColor:'gray',
    width:wp('42%'),
    justifyContent:'center',
    alignItems:'center',
    borderBottomRightRadius:wp('3%')
  },
  btnText:{
    fontSize:14,
    color:'white'
  }
})