import React from 'react'
import {View,Text,StyleSheet,ImageBackground, TouchableOpacity,Image,Button} from 'react-native'
import { MaterialIcons } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants'; 
const HomeScreen =(props)=>{
  
  const doSubmit = () => {
    fetch('https://ezzyin.ausnep.com/api/business/contacts/10?id=10', {
      method: 'GET', // or 'PUT'
      headers: {
        'accept': 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then(data => {
        console.log('Home Screen:', data);
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  }
    return(
        <View style={styles.container}>
            <View style={styles.headerLine}></View>
             <View style={styles.advertise}>
               <View style={styles.topHeader}>
                   <Text style={{color:'gray',marginRight:wp('1%')}}>Ads</Text>
                   <TouchableOpacity>
                      <MaterialIcons name="cancel" size={26} color="#C1C0C9" />
                   </TouchableOpacity>
               </View>
               
               <ImageBackground source={require('../assets/restraurant.png')} style={styles.bgImage}>
               </ImageBackground>

              <View style={{flexDirection:'row',justifyContent:'space-between',width:wp('84%'),marginTop:hp('1%')}}>
                <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                     <MaterialCommunityIcons name='map-marker' size={15}/>
                     <Text style={{marginLeft:wp('1.5%'),fontWeight:'bold'}}>S-14 Sekhar</Text>
                 </View>
                 <View style={{borderRightColor:'#C1C0C9',borderRightWidth:2}}></View>
                 <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                     <MaterialCommunityIcons name='phone-outline' size={15}/>
                      <Text style={{marginLeft:wp('1.5%'),fontWeight:'bold'}}>9860552717</Text>
                 </View>
                 <View style={{borderRightColor:'#C1C0C9',borderRightWidth:2}}></View>

                 <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}> 
                    <View style={{marginRight:wp('1%'),height:20,width:20,borderRadius:10,backgroundColor:'gray',justifyContent:'center',alignItems:'center'}}>
                      <MaterialCommunityIcons name='twitter'color={'white'} />
                    </View>
                    <View style={{marginRight:wp('1%'),height:20,width:20,borderRadius:10,backgroundColor:'gray',justifyContent:'center',alignItems:'center'}}>
                    <MaterialCommunityIcons name='facebook'color={'white'} />
                    </View>
                    <View style={{height:20,width:20,borderRadius:10,backgroundColor:'gray',justifyContent:'center',alignItems:'center'}}>
                    <MaterialCommunityIcons name='instagram' color={'white'}/>
                    </View>
                 </View>
               </View>
             </View>

             <View style={{justifyContent:'center',alignItems:'center',marginTop:hp('2%')}}>
                 <Image source={require('../assets/qr.png')} style={styles.qrImage} />
             </View>

             <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:hp('4%'),backgroundColor:'white',elevation:4,padding:14,borderRadius:8,width:wp('84%')}}>
                <Text style={{fontSize:20,fontWeight:'bold',marginLeft:wp('2%')}}>Recent Activity</Text>
                <TouchableOpacity onPress={()=>props.navigation.navigate('RecentActivity')}>
                   <MaterialCommunityIcons name="chevron-right" size={30}  />
                </TouchableOpacity>
             </View>
             <Button 
             title="Click me"
             onPress={()=>doSubmit()}
             />
             <Text></Text>

        </View>
    )
}
export default HomeScreen
const styles = StyleSheet.create({
    container:{
        flex:1,
        marginTop:Constants.statusBarHeight,
        paddingHorizontal:wp('8%'),
        backgroundColor:'white'
    },
    headerLine:{
      marginTop:hp('7%'),
      borderBottomColor:'#C1C0C9',
      borderBottomWidth:1,
    },
    advertise:{
      marginVertical:hp('3%')
    },
    topHeader:{
      flexDirection:'row',
      justifyContent:'flex-end',
    },
    bgImage:{
      width:wp('84%'),
      height:150,
    },
      qrImage:{
       height:200,
       width:200,
  } ,

})