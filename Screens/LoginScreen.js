import React, { useState } from 'react'
import { View, StyleSheet, Text, TextInput, TouchableHighlight, Switch } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
function LoginScreen(props) {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [status, setIsEnabled] = useState(false);
    const toggleSwitch = () => setIsEnabled(previousState => !previousState);

    const loginUser = () => {
        const data = {
            user_id: 2,
            email: email,
            password: password,
            status: status,
        };
        fetch('https://ezzyin.ausnep.com/api/login', {
            method: 'POST', // or 'PUT'
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        })
            .then(response => response.json())
            .then(data => {
                console.log('Success Login:', data);
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }

    return (
        <View style={styles.container}>
            <View style={styles.mainHeader}>
                <Text style={styles.headerText}>Welcome <Text style={{ fontSize: wp('14%'), color: '#C1C0C9' }}>back !</Text></Text>
            </View>

            <View style={styles.loginForm}>
                <TextInput
                    style={styles.inputText}
                    placeholder="  Username"
                    placeholderTextColor="#A9A8A6"
                    value={email}
                    onChangeText={email => setEmail(email)} />
                <TextInput
                    secureTextEntry
                    style={styles.inputText}
                    placeholder=" Password"
                    value={password}
                    placeholderTextColor="#A9A8A6"
                    onChangeText={password => setPassword(password)} />
                <Switch
                    trackColor={{ false: "#767577", true: "#81b0ff" }}
                    thumbColor={status ? "#f5dd4b" : "#f4f3f4"}
                    ios_backgroundColor="#3e3e3e"
                    onValueChange={toggleSwitch}
                    value={status}
                    onChangeText={status => setIsEnabled(status)}
                />
            </View>
            <View style={styles.termsText}>
                <Text style={styles.termsTextContent}>By registering and signing in, You agree our terms and conditions</Text>
            </View>
            <View style={styles.btn}>
                <TouchableHighlight style={styles.loginBtn} onPress={() => { loginUser(); props.navigation.navigate('Home') }}>
                    <Text style={styles.loginText}>SUBMIT</Text>
                </TouchableHighlight>
                <Text style={styles.forgotText} onPress={() => props.navigation.navigate('ForgotPassword')}>Forgot Your Password</Text>
            </View>
        </View>
    )
}
export default LoginScreen
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'white'
    },
    mainHeader: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        width: wp('82%')
    },
    headerText: {
        fontSize: wp('10%'),
        fontWeight: 'bold'
    },
    loginForm: {
        width: wp('80%')
    },
    inputText: {
        height: hp('6%'),
        color: "#C1C0C9",
        borderColor: '#C1C0C9',
        borderWidth: wp('0.3%'),
        borderRadius: wp('1%'),
        fontSize: wp('4%'),
        marginVertical: hp('1%')
    },
    termsText: {
        width: wp('78%'),
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: hp('10%')
    },
    termsTextContent: {
        textAlign: 'center',
        color: '#C1C0C9',
        fontSize: wp('3%')
    },
    btn: {
        width: wp('80%'),
        justifyContent: 'center',
        alignItems: 'center'
    },
    loginBtn: {
        backgroundColor: '#696969',
        paddingTop: hp("1.7%"),
        paddingLeft: wp('2.7%'),
        paddingRight: wp('2.7%'),
        paddingBottom: hp('1.7%'),
        width: "100%",
        borderRadius: wp('3.5%')
    },
    loginText: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: wp('3%')
    },
    forgotText: {
        marginTop: hp('1.5%'),
        color: '#C1C0C9',
        marginBottom: hp('3%')

    }


})