import * as React from 'react';
import { View, Button,Image,Text,StyleSheet } from 'react-native';
import {
  NavigationContainer,
  getFocusedRouteNameFromRoute,
} from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import RegisterScreen from './RegisterScreen'
import LoginScreen from './LoginScreen'
import ForgotPassword from './ForgotPassword'
import EnterCode from './EnterCode'
import NewPassword from './NewPassword'

const Stack = createStackNavigator();

export default function StackNavigator() {

  return (
    <NavigationContainer>
      <Stack.Navigator  screenOptions={{headerTitle: ''}} >
      <Stack.Screen name="LoginScreen" component={LoginScreen}  />
        <Stack.Screen name="RegisterScreen" component={RegisterScreen} />   
        <Stack.Screen name="ForgotPassword" component={ForgotPassword} /> 
        <Stack.Screen name="EnterCode" component={EnterCode} />    
        <Stack.Screen name="NewPassword" component={NewPassword} /> 
      </Stack.Navigator>
    </NavigationContainer>
  );
}
