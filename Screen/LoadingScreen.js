import  React,{useEffect} from 'react';
import { Text, View,Button,Image } from 'react-native';
function LoadingScreen({navigation}) {
    useEffect(() => {
        setTimeout(() => {
            navigation.navigate("Onboarding")
        }, 2000);
      }, []);
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center',backgroundColor:'white' }}>
        <Text style={{fontSize:35,fontWeight:'bold',color:'#262628',fontStyle:'italic'}}>Covid Tracker</Text>
       </View>
    );
  }
  export default LoadingScreen