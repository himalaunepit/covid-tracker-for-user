import React, { useState } from 'react'
import {View,Text,StyleSheet,TextInput,TouchableHighlight} from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
function NewPassword(props){
  const [password,setPassword] = useState('')
  const [newPassword,setNewPassword] = useState('')
  return(
      <View style={styles.mainContainer}>
          <View style={styles.mainHeader}>
            <Text style={styles.firstText}>Enter</Text>
            <Text style={styles.secondText}><Text style={{color:'#262628'}}>New</Text> Password </Text>
          </View>

  
          <View style={styles.form}>
             <TextInput  
             secureTextEntry
              style={styles.inputText}
              placeholder="  New Password" 
              placeholderTextColor="#A9A8A6"
              onChangeText={text =>setPassword({email:text})}/>
               <TextInput  
               secureTextEntry
              style={styles.inputText}
              placeholder="  Retype New Password" 
              placeholderTextColor="#A9A8A6"
              onChangeText={text =>setNewPassword({email:text})}/>
          </View>

    
          <View style={styles.btn}>
                            <TouchableHighlight style={styles.loginBtn} onPress={()=>props.navigation.navigate("Home")}>
                    <Text style={styles.loginText}>SUBMIT</Text>
             </TouchableHighlight>
          </View>
      </View>
  )
}
export default NewPassword

const styles = StyleSheet.create({
  mainContainer:{
        backgroundColor:'white',
        justifyContent:'flex-start',
        alignItems:'center',
        width:wp('100%'),
        height:hp('100%'),
  } ,
  mainHeader:{
    width:wp('80%'),
    alignItems:'flex-start',
    justifyContent:'flex-end',
    height:hp('15%'),
  },
  firstText:{
     fontSize:hp('5%'),
     fontWeight:'bold',
     color:'#C1C0C9',
  },
  secondText:{
    fontSize:hp('5%'),
    fontWeight:'bold',
    color:'#C1C0C9',
  },
  form:{
    height:hp('25%'),
    justifyContent:'flex-end',
    width:wp('80%'),
  },
    inputText:{
    height:hp('6%'),
    color:"#C1C0C9",
    borderColor: '#C1C0C9', 
    borderWidth: wp('0.3%'), 
    borderRadius:wp('1%'),
    fontSize:wp('4%'),
    marginVertical:hp('1%'),
  },
 
  btn:{
    height:hp('45%'),
    justifyContent:'flex-end',
    width:wp('80%')
  },
  loginBtn:{
    backgroundColor:'#696969', 
    paddingTop:hp("1.7%"), 
    paddingLeft:wp('2.7%'), 
    paddingRight:wp('2.7%'),
    paddingBottom:hp('1.7%'),
    width:"100%",
    borderRadius:wp('3.5%')
  },
  loginText:{
    color:'white', 
    fontWeight:'bold',
    textAlign:'center',
    fontSize:wp('3%')
  },
})