import React,{useState} from 'react';
import { StyleSheet, Text, View, TextInput,TouchableHighlight } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
export default function RegisterForCompany(props) {
    const [email,setEmail]=useState('');
    const [pass,setPass]=useState('');
    const [confirmpass,setConfirmpass] =useState('');
    
    registerUser=()=>{
        fetch('ezzyin.ausnep.com/api/register', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            compname:compname,
            abn:abn,
            name:name,
            address:address,
            phone:phone,
            image:image,
            email:email,
            pass:pass,
            confirmpass:confirmpass

        })
});
    }
    return (
      <View style={styles.container}>
          
        <View style={styles.mainheader}>
            <Text style={{fontSize:wp('9%'),fontWeight:'bold',color:'#262628'}}>Join</Text>
        </View>

        <View style={styles.subheder}>
        <Text style={{fontSize:wp('6%'),color:'#C1C0C9',fontWeight:'bold'}}>For Full Featured Experience</Text>
        </View>       
        <View style={styles.TextInput}>
        <TextInput  
            style={styles.inputText}
            placeholder="Email" 
            placeholderTextColor="#C1C0C9"
            onChangeText={text => setEmail({email:text})}/>
        </View>
      
        <View style={styles.TextInput}>
        <TextInput  
            style={styles.inputText}
            secureTextEntry
            placeholder="Password" 
            placeholderTextColor="#C1C0C9"
            onChangeText={text => setPass({pass:text})}/>
        </View>
     
           
        <View style={styles.TextInput}>
        <TextInput  
           secureTextEntry
            style={styles.inputText}
            placeholder="Verify Password" 
            placeholderTextColor="#C1C0C9"
            onChangeText={text => setConfirmpass({confirmpass:text})}/>
        </View>   
    
        <View style={styles.btnstyle}>
                <TouchableHighlight style={styles.loginBtn} onPress={()=>props.navigation.navigate('LoginScreen')}>
                        <Text style={styles.loginText}>REGISTER</Text>
                </TouchableHighlight>
        </View>

        <View style={styles.textcontent}>
           <Text style={styles.smalltext}>By clicking Register,You agree to our terms and condition,data policy and Cookies policy,You may receive SMS Notifications from us and can opt out any time
           .
           </Text>
        </View>
      </View>
    );
}

const styles = StyleSheet.create({
    container:{
        // flex:1,
        height:hp('100%'),
        backgroundColor:'white',
        flexDirection:'column',
        justifyContent:'flex-start',
        alignItems:'center',
        
    },
    mainheader:{
        // flex:1,
        height:hp('8%'),
        width:wp('80%'),
        justifyContent:'flex-end'
    },
    subheder:{
        // flex:1,
        height:hp('8%'),
        width:wp('80%'),
        justifyContent:'center',
    },

    textcontent:{
        // flex:1,
        height:hp('10%'),
        width:wp('80%'),
        justifyContent:'center',
        alignItems:'flex-start',
    },
    smalltext:{
        fontSize:wp('3%'),
        color:'#C1C0C9',
    },
    TextInput:{
        // flex:1,
        height:hp('10%'),
        width:wp('80%'),
        justifyContent:'center'
    },
    inputText:{
        color:"#A9A8A6",
        borderBottomColor: '#C1C0C9',
        borderBottomWidth: wp('0.4%') ,
        fontSize:wp('4%'),
    },
    btnstyle:{
        // flex:1,
        height:hp('35%'),
        width:wp("80%"),
        justifyContent:'flex-end',
    },
    loginBtn:{
        backgroundColor:'#696969', 
        paddingTop:hp("1.7%"), 
        paddingLeft:wp('2.7%'), 
        paddingRight:wp('2.7%'),
        paddingBottom:hp('1.7%'),
        width:"100%",
        borderRadius:wp('3.5%')
      },
      loginText:{
        color:'white', 
        fontWeight:'bold',
        textAlign:'center',
        fontSize:wp('3%')
      },


})