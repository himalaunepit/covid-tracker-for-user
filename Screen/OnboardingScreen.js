import * as React from 'react';
import {View,Text,Button,StyleSheet,Image,TouchableOpacity} from 'react-native'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Onboarding from 'react-native-onboarding-swiper';
const Done =({...props}) =>(
    <TouchableOpacity
    style={{marginHorizontal:wp('3%')}}
    {...props}
    >
     <Text style={{fontSize:wp('4%')}}>Done</Text>
    </TouchableOpacity>
)

const Dots = ({selected}) => {
    let backgroundColor;

    backgroundColor = selected ? 'white' : 'white';

    return (
        <View 
            style={{
                width:wp('1.5%'),
                height: wp('1.5%'),
                marginHorizontal: wp('0.7%'),
                backgroundColor
            }}
        />
    );
}
const OnboardingScreen = ({navigation}) => {
    return(   
        <Onboarding
        DotComponent={Dots}
        DoneButtonComponent={Done}
        showNext={false}
        showSkip={true}
        // onSkip={()=>navigation.navigate("RegisterScreen")}
        onDone={()=>navigation.navigate("RegisterScreen")}
        titleStyles={{ color: 'black',fontSize:wp('5%'),fontWeight:'bold',width:wp('60%')  }}
        subTitleStyles={{ color: 'black',fontSize:wp('3%'),alignItems:'flex-start',width:wp('60%') }}
        pages={[
            {
            backgroundColor: 'white',
            image: <Image source={require('../assets/onboardingscreen.png')} />,
            title: 'Scan Your Profile',
            subtitle: 'We make it simple to find the latest deal for you .Enter your address ',
            },
            {
                backgroundColor: 'white',
                image: <Image source={require('../assets/Bitmap.png')} />,
                title: 'Get exclusive ',
                subtitle: 'When you buy deals,we will hook you up with exclusive cupon,specials',
            },
            {
                backgroundColor: 'white',
                image: <Image source={require('../assets/Layer.png')}/>,
                title: 'Pick Up Or ',
                subtitle: 'We make food ordering fast,simple and free',
            },

        ]} 
        />
    );
};
export default OnboardingScreen;

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
});