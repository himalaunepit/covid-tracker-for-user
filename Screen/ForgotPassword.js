import React, { useState } from 'react'
import {View,Text,StyleSheet,TextInput,TouchableHighlight} from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
function ForgotPassword(props){
  const [email,setEmail] = useState('')
  return(
      <View style={styles.mainContainer}>
          <View style={styles.mainHeader}>
            <Text style={styles.firstText}>Forgot</Text>
            <Text style={styles.secondText}>Password </Text>
            <MaterialCommunityIcons name='key' size={hp('5%')}/>
          </View>

          <View style={styles.textContent}>
            <Text style={styles.textStyle}>Enter the Email Address used to register this account.</Text>
             <Text style={styles.textStyle}>We will send you a link to change a password</Text>
          </View>

          <View style={styles.form}>
             <TextInput  
              style={styles.inputText}
              placeholder="  Email" 
              placeholderTextColor="#A9A8A6"
              onChangeText={text =>setEmail({email:text})}/>
          </View>

          <View style={styles.resendText}>
            <Text style={styles.resendContent}>Resend Link</Text>
          </View>

          
          <View style={styles.btn}>
                            <TouchableHighlight style={styles.loginBtn} onPress={()=>props.navigation.navigate("EnterCode")}>
                    <Text style={styles.loginText}>SUBMIT</Text>
             </TouchableHighlight>
          </View>
      </View>
  )
}
export default ForgotPassword

const styles = StyleSheet.create({
  mainContainer:{
        backgroundColor:'white',
        justifyContent:'flex-start',
        alignItems:'center',
        width:wp('100%'),
        height:hp('100%'),
  } ,
  mainHeader:{
    flexDirection:'row',
    width:wp('82%'),
    alignItems:'flex-end',
    height:hp('9%'),
  },
  firstText:{
     fontSize:wp('7%'),
     fontWeight:'bold'
  },
  secondText:{
    fontSize:wp('9%'),
    fontWeight:'bold',
    color:'#C1C0C9',
    marginLeft:hp('2%')

  },
  textContent:{
    height:hp('15%'),
    justifyContent:'flex-end',
    width:wp('78%')
  },
  form:{
    height:hp('10%'),
    justifyContent:'flex-end',
    width:wp('80%')
  },
    inputText:{
    height:hp('6%'),
    color:"#C1C0C9",
    borderColor: '#C1C0C9', 
    borderWidth: wp('0.3%'), 
    borderRadius:wp('1%'),
    fontSize:wp('4%'),
    marginVertical:hp('1%'),
  },
  resendText:{
    height:hp('4%'),
    width:wp('80%'),
    alignItems:'flex-end',
    justifyContent:'center'
  },
  resendContent:{
      fontWeight:'bold',
      color:'#C1C0C9',
      fontSize:hp('2%')
  },
  btn:{
    height:hp('49%'),
    justifyContent:'flex-end',
    width:wp('80%')
  },
  loginBtn:{
    backgroundColor:'#696969', 
    paddingTop:hp("1.7%"), 
    paddingLeft:wp('2.7%'), 
    paddingRight:wp('2.7%'),
    paddingBottom:hp('1.7%'),
    width:"100%",
    borderRadius:wp('3.5%')
  },
  loginText:{
    color:'white', 
    fontWeight:'bold',
    textAlign:'center',
    fontSize:wp('3%')
  },
  textStyle:{
    color:'#C1C0C9',
    fontSize:wp('3%')
  }
})
