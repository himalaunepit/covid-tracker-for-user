import React,{useState,useEffect} from 'react';
import { StyleSheet, Text, View, TextInput,TouchableHighlight ,Button,Image,Platform} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import { add } from 'react-native-reanimated';
export default function RegisterScreen(props)  {
  const [name,setName] =useState('');
    const [company_name,setCompname]=useState('');
    const [abn,setAbn]=useState('');
    const [email,setEmail]=useState('');
    const [password,setPass]=useState('');
    const [password_confirmation,setVPass]=useState('');
    const [address,setAddress]=useState('');
    const [phone,setPhone]=useState('');
    const [logo, setImage] = useState(null);
  
    useEffect(() => {
      (async () => {
        if (Platform.OS !== 'web') {
          const { status } = await ImagePicker.requestCameraRollPermissionsAsync();
          if (status !== 'granted') {
            alert('Sorry, we need camera roll permissions to make this work!');
          }
        }
      })();
    }, []);
  
    const pickImage = async () => {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
        allowsEditing: true,
        aspect: [4, 3],
        quality: 1,
      });
  
      console.log(result);
  
      if (!result.cancelled) {
        setImage(result.uri);
      }
    };


    const registerUser = () => {
      const data = { 
        company_name:company_name,
        name:name,
        abn:abn,
        email:email,
        password:password,
        password_confirmation:password_confirmation,
        address:address,
        phone:phone,
        logo:logo};
      fetch('http://ezzyin.ausnep.com/api/register', {
          method: 'POST', // or 'PUT'
          headers: {
              Accept:'application/json',
              'Content-Type': 'multipart/form-data',
          },
          body: JSON.stringify(data),
      })
          .then(response => response.json())
          .then(data => {
              console.log('Success Register:', data);
          })
          .catch((error) => {
              console.error('Error:', error);
          });
  }

    return (
      <View style={styles.container}>
          
        <View style={styles.mainheader}>
            <Text style={{fontSize:wp('12%'),fontWeight:'bold',color:'#262628'}}>Join</Text>
        </View>

        <View style={styles.subheder}>
        <Text style={{fontSize:wp('12%'),color:'#C1C0C9',fontWeight:'bold'}}>Covid Tracker</Text>
        </View>

        <View style={styles.TextInput}>
            <TextInput  
                style={styles.inputText}
                placeholder="Company Name" 
                value={company_name}
                placeholderTextColor="#C1C0C9"
                onChangeText={company_name => setCompname(company_name)}/>
        </View>
        
      
        <View style={styles.TextInput}>
        <TextInput  
            style={styles.inputText}
            placeholder="Contact Person" 
            placeholderTextColor="#C1C0C9"
            value={name}
            onChangeText={name => setName(name)}/>
        </View>

        <View style={styles.TextInput}>
        <TextInput  
            style={styles.inputText}
            placeholder="ABN/ACN" 
            value={abn}
            placeholderTextColor="#C1C0C9"
            onChangeText={abn => setAbn(abn)}/>
        </View>

        <View style={styles.TextInput}>
        <TextInput  
            style={styles.inputText}
            placeholder="Email" 
            value={email}
            placeholderTextColor="#C1C0C9"
            onChangeText={email => setEmail(email)}/>
        </View>
      
        <View style={styles.TextInput}>
        <TextInput  
            style={styles.inputText}
            secureTextEntry
            placeholder="Password" 
            value={password}
            placeholderTextColor="#C1C0C9"
            onChangeText={password => setPass(password)}/>
        </View>
         
        <View style={styles.TextInput}>
        <TextInput  
            style={styles.inputText}
            secureTextEntry
            placeholder="Verify Password" 
            value={password_confirmation}
            placeholderTextColor="#C1C0C9"
            onChangeText={password_confirmation => setVPass(password_confirmation)}/>
        </View>
  
        <View style={styles.TextInput}>
        <TextInput  
      
            style={styles.inputText}
            value={address}
            placeholder="Adress" 
            placeholderTextColor="#C1C0C9"
            onChangeText={address =>setAddress(address)}/>
        </View>   

        <View style={styles.TextInput}>
        <TextInput  
            style={styles.inputText}
            placeholder="Phone Number" 
            value={phone}
            placeholderTextColor="#C1C0C9"
            onChangeText={phone => setPhone(phone)}/>
        </View>   
        <View style={{width:wp('80%'),marginTop:hp('1%')}}>
           <Button title="Pick a Logo " onPress={pickImage} />
           {logo && <Image source={{ uri:logo }} style={{ width: 100, height: 100 }} />}
        </View>
        <View style={styles.btnstyle}>
                <TouchableHighlight style={styles.loginBtn} onPress={()=>{registerUser();props.navigation.navigate('LoginScreen')}}>
                        <Text style={styles.loginText}>CONTINUE</Text>
                </TouchableHighlight>
        </View>
      </View>
    );
  }

const styles = StyleSheet.create({
    container:{
        // flex:1,
        height:hp('100%'),
        backgroundColor:'white',
        flexDirection:'column',
        justifyContent:'flex-start',
        alignItems:'center',
        
    },
    mainheader:{
        // flex:1,
        height:hp('8%'),
        width:wp('80%'),
        justifyContent:'flex-end'
    },
    subheder:{
        // flex:1,
        height:hp('8%'),
        width:wp('80%'),
        justifyContent:'center',
    },
    TextInput:{
        // flex:1,
        height:hp('3%'),
        width:wp('80%'),
        justifyContent:'center'
    },
    inputText:{
        color:"#A9A8A6",
        borderBottomColor: '#C1C0C9',
        borderBottomWidth: wp('0.4%') ,
        fontSize:wp('4%'),
    },
    btnstyle:{
        // flex:1,
        height:hp('28%'),
        width:wp("80%"),
        justifyContent:'flex-end',
    },
    loginBtn:{
        backgroundColor:'#696969', 
        paddingTop:hp("1.7%"), 
        paddingLeft:wp('2.7%'), 
        paddingRight:wp('2.7%'),
        paddingBottom:hp('1.7%'),
        width:"100%",
        borderRadius:wp('3.5%'),
        marginBottom:hp('5%')
      },
      loginText:{
        color:'white', 
        fontWeight:'bold',
        textAlign:'center',
        fontSize:wp('3%')
      },


})